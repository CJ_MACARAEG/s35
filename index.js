const express = require ("express")
const mongoose = require ("mongoose")
const dotenv = require ("dotenv")

dotenv.config()

const app = express()
const port = 3005


mongoose.connect(`mongodb+srv://cjmacaraeg900:${process.env.MONGODB_PASSWORD}@cluster0.it74nuq.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


let db = mongoose.connection
db.on('error', () => console.error("Connection Error"))
db.on('open', () => console.error("Connected to MongoDB!"))
// MongoDB Connection End


app.use(express.json())
app.use(express.urlencoded({extended: true}))

// MongoDB Schemas
const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	status: {
		type: String
	}
})
// MongoDB Schemas END

// MongoDB Model
const User = mongoose.model('User', userSchema)
// MongoDB Model END

// Routes
app.post('/signup', (request, response) => {
	User.findOne({username: request.body.username, password: request.body.password}, (error, result) => {
		if(result != null && result.username == request.body.username && result != null && result.password == request.body.password ){
			return response.send('Duplicate task found!')
		}

		let newUser = new User({
			username: request.body.username,
			password: request.body.password
		})

		newUser.save((error, savedUser) => {
			if(error){
				return console.error(error)
			}
			else {
				return response.status(200).send('New user registered!')
			}
		})
	})
})


// app.get('/tasks', (request, response) => {
// 	Task.find({}, (error, result) => {
// 		if(error){
// 			return console.log(error)
// 		}

// 		return response.status(200).json({
// 			data: result
// 		})
// 	})
// })
// Routes END

app.listen(port, () => console.log(`Server running at local host:${port}`))
